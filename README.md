# smokeping-role

An Ansible role for deploying smokeping using docker-compose from:
https://hub.docker.com/r/linuxserver/smokeping

# Input Variables

* `smokeping_uid` -- A User ID that will be used by the smokeping process. If none provided, it'll
  default to `1000`.
* `smokeping_gid` -- A Group ID that will be used by the smokeping process. If none provided, it'll
  default to `1000`.
* `smokeping_main_dir` -- A directory that will be have 2 subdirectories: `config` and `data`.
  Those will mounted inside the Docker container as `/config` and `/data` respectively.
* `smokeping_timezone` -- A time zone that will be used in smokeping's config. If not provided,
  defaults to `Europe/London`.
* `smokeping_host_port` -- A TCP port on the host at which the HTTP interface will be exposed.
  If not provided, it'll default to `80`.
